import { createConnections } from "typeorm";
import path from "path";

const createConnectionDB = async () => {
  try {
    await createConnections([
      {
        name: "default",
        type: "mssql",
        host: process.env.MSSQL_HOST,
        port: Number(process.env.MSSQL_PORT),
        username: process.env.MSSQL_USER,
        password: process.env.MSSQL_PASS,
        database: process.env.MSSQL_DB,
        stream: true,
        options: {
          encrypt: process.env.MSSQL_ENCRYPT === "true",
        },
        synchronize: false,
        logging: true,
        maxQueryExecutionTime: 3000,
        cache: {
          type: "redis",
          duration: Number(process.env.REDIS_TTL) * 1000,
          options: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            password: process.env.REDIS_PASS,
          },
        },
        entities: [
          path.resolve(__dirname, "..", "models", "dwh", "**", "*.{ts,js}"),
        ],
        migrations: [
          path.resolve(
            __dirname,
            "..",
            "database",
            "migration",
            "dwh",
            "**",
            "*.{ts,js}",
          ),
        ],
        subscribers: [
          path.resolve(__dirname, "..", "subscriber", "dwh", "**", "*.{ts,js}"),
        ],
        cli: {
          entitiesDir: path.resolve("..", "models", "dwh"),
          migrationsDir: path.resolve("..", "migration", "dwh"),
          subscribersDir: path.resolve("..", "subscriber", "dwh"),
        },
      },
    ]);
  } catch (err) {
    console.log(err);
  }
};

createConnectionDB();
