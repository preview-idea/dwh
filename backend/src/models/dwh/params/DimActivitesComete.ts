import { ViewColumn, ViewEntity } from "typeorm";

@ViewEntity("v_dim_activites_comete", { schema: "dbo", database: "dwh_seris" })
export class DimActivitesComete {
  @ViewColumn({ name: "id_activite" })
  idActivite: number;

  @ViewColumn({ name: "li_activite" })
  liActivite: string;

  @ViewColumn({ name: "li_code_activite" })
  liCodeActivite: string;

  @ViewColumn({ name: "is_facturable" })
  isFacturable: boolean;
}
