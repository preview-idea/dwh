import { ViewColumn, ViewEntity } from "typeorm";

@ViewEntity("v_dim_sites", { schema: "dbo", database: "dwh_seris" })
export class DimSites {
  @ViewColumn({ name: "id_site" })
  idSite: number;

  @ViewColumn({ name: "li_site" })
  liSite: string;

  @ViewColumn({ name: "li_site_ville" })
  liSiteVille: string;
}
