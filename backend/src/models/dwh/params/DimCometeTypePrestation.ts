import { ViewColumn, ViewEntity } from "typeorm";

@ViewEntity("v_dim_comete_type_prestation", { schema: "dbo", database: "dwh_seris" })
export class DimCometeTypePrestation {
  @ViewColumn({ name: "id_type_prestation" })
  idTypePrestation: number;

  @ViewColumn({ name: "li_type_prestation" })
  liTypePrestation: string;
}
