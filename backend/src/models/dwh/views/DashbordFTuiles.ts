import { Column, Entity, Index } from "typeorm";

@Index(
  "pk_dashbord_f_tuiles",
  ["idTuile", "idTypeIndicateur", "idNiveau", "idAgenceAx"],
  { unique: true }
)
@Entity("dashbord_f_tuiles", { schema: "dbo" })
export class DashbordFTuiles {
  @Column("int", { primary: true, name: "id_tuile" })
  idTuile: number;

  @Column("int", { primary: true, name: "id_type_indicateur" })
  idTypeIndicateur: number;

  @Column("nvarchar", {
    name: "li_type_indicateur",
    nullable: true,
    length: 50,
  })
  liTypeIndicateur: string | null;

  @Column("int", { primary: true, name: "id_niveau" })
  idNiveau: number;

  @Column("nvarchar", { primary: true, name: "id_agence_ax", length: 3 })
  idAgenceAx: string;  

  @Column("decimal", { name: "nb_valeur", precision: 17, scale: 2 })
  nbValeur: number;

  @Column("int", { name: "id_agence" })
  idAgence: number;
  
  @Column("int", { name: "id_alerte" })
  idAlerte: number;

  @Column("datetime", { name: "dtload" })
  dtload: Date;

  @Column("nvarchar", { name: "dbname", length: 30 })
  dbname: string;
  
  @Column("date", { name: "dt_debut_rapport", nullable: true })
  dtDebutRapport: Date | null;
  
  @Column("date", { name: "dt_fin_rapport", nullable: true })
  dtFinRapport: Date | null;
}