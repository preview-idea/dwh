import { Column, Entity, Index } from "typeorm";

@Index("PK__RAPPORT___5B7BF721B334FF9E", ["idContrat", "idMatricule"], {
  unique: true,
})
@Entity("RAPPORT_f_STC", { schema: "dbo" })
export class RapportFStc {
  @Column("int", { primary: true, name: "id_matricule" })
  idMatricule: number;

  @Column("int", { primary: true, name: "id_contrat" })
  idContrat: number;

  @Column("varchar", { name: "li_nom", nullable: true, length: 30 })
  liNom: string | null;

  @Column("varchar", { name: "li_prenom", nullable: true, length: 30 })
  liPrenom: string | null;

  @Column("int", { name: "id_agence", nullable: true })
  idAgence: number | null;

  @Column("char", { name: "li_code_agence_ax", nullable: true, length: 3 })
  liCodeAgenceAx: string | null;

  @Column("varchar", { name: "li_agence", nullable: true, length: 60 })
  liAgence: string | null;

  @Column("int", { name: "id_bu", nullable: true })
  idBu: number | null;

  @Column("varchar", { name: "li_bu", nullable: true, length: 20 })
  liBu: string | null;

  @Column("varchar", { name: "id_region", nullable: true, length: 30 })
  idRegion: string | null;

  @Column("varchar", { name: "li_region", nullable: true, length: 60 })
  liRegion: string | null;

  @Column("varchar", { name: "li_typecontrat", nullable: true, length: 15 })
  liTypecontrat: string | null;

  @Column("date", { name: "dt_debut_contrat", nullable: true })
  dtDebutContrat: Date | null;

  @Column("date", { name: "dt_fin_contrat", nullable: true })
  dtFinContrat: Date | null;

  @Column("varchar", {
    name: "li_motif_fin_contrat",
    nullable: true,
    length: 85,
  })
  liMotifFinContrat: string | null;

  @Column("datetime", { name: "dt_cloture_mosaic", nullable: true })
  dtClotureMosaic: Date | null;

  @Column("datetime", { name: "dt_cloture_comete", nullable: true })
  dtClotureComete: Date | null;

  @Column("datetime", { name: "dt_cloture_agence_complete", nullable: true })
  dtClotureAgenceComplete: Date | null;

  @Column("int", { name: "nb_delai_cloture_complete", nullable: true })
  nbDelaiClotureComplete: number | null;

  @Column("date", { name: "dt_theorique_envoie_rhpi", nullable: true })
  dtTheoriqueEnvoieRhpi: Date | null;

  @Column("date", { name: "dt_envoie_rhpi", nullable: true })
  dtEnvoieRhpi: Date | null;

  @Column("int", { name: "nb_envoie_possible_rhpi", nullable: true })
  nbEnvoiePossibleRhpi: number | null;

  @Column("int", {
    name: "nb_delai_chargement_rhpi_apres_cloture",
    nullable: true,
  })
  nbDelaiChargementRhpiApresCloture: number | null;

  @Column("int", { name: "nb_delai_theorique_virement", nullable: true })
  nbDelaiTheoriqueVirement: number | null;

  @Column("date", { name: "dt_theorique_virement", nullable: true })
  dtTheoriqueVirement: Date | null;

  @Column("date", { name: "dt_reelle_virement", nullable: true })
  dtReelleVirement: Date | null;

  @Column("int", { name: "nb_delai_traitement_paie", nullable: true })
  nbDelaiTraitementPaie: number | null;

  @Column("int", { name: "nb_delai_virement_stc", nullable: true })
  nbDelaiVirementStc: number | null;

  @Column("int", { name: "nb_sla_traitement_paie", nullable: true })
  nbSlaTraitementPaie: number | null;

  @Column("int", { name: "nb_sla_virement_salarie", nullable: true })
  nbSlaVirementSalarie: number | null;

  @Column("int", { name: "id_periode", nullable: true })
  idPeriode: number | null;

  @Column("date", { name: "li_periode", nullable: true })
  liPeriode: Date | null;

  @Column("varchar", { name: "li_code_societe_ax", nullable: true, length: 40 })
  liCodeSocieteAx: string | null;

  @Column("nvarchar", { name: "DBNAME", nullable: true, length: 29 })
  dbname: string | null;

  @Column("int", { name: "ROWNUM", nullable: true })
  rownum: number | null;

  @Column("datetime", { name: "DTLOAD", nullable: true })
  dtload: Date | null;

  @Column("int", { name: "nb_stc_clos", nullable: true })
  nbStcClos: number | null;
}
