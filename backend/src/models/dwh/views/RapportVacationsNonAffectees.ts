import { ViewColumn, ViewEntity } from "typeorm";

@ViewEntity("v_rapport_vacations_non_affectees", {
  schema: "dbo",
  database: "dwh_seris",
})
export class RapportVacationsNonAffectees {
  @ViewColumn({ name: "id_vacation" })
  idVacation: number;

  @ViewColumn({ name: "id_bu" })
  idBu: number;

  @ViewColumn({ name: "li_bu" })
  liBu: string;

  @ViewColumn({ name: "id_ddr_ax" })
  idDdrAx: string;

  @ViewColumn({ name: "id_region" })
  idRegion: string;

  @ViewColumn({ name: "li_region" })
  liRegion: string;

  @ViewColumn({ name: "li_code_agence_ax" })
  liCodeAgenceAx: string;

  @ViewColumn({ name: "id_agence" })
  idAgence: number;

  @ViewColumn({ name: "li_agence" })
  liAgence: string;

  @ViewColumn({ name: "id_periode" })
  idPeriode: number;

  @ViewColumn({ name: "li_periode" })
  liPeriode: Date;

  @ViewColumn({ name: "li_code_societe_ax" })
  liCodeSocieteAx: string;

  @ViewColumn({ name: "li_societe" })
  liSociete: string;

  @ViewColumn({ name: "id_client" })
  idClient: number;

  @ViewColumn({ name: "li_client" })
  liClient: string;

  @ViewColumn({ name: "id_site" })
  idSite: number;

  @ViewColumn({ name: "li_site" })
  liSite: string;

  @ViewColumn({ name: "id_prestation" })
  idPrestation: number;

  @ViewColumn({ name: "li_prestation" })
  liPrestation: string;

  @ViewColumn({ name: "dt_vacation" })
  dtVacation: Date;

  @ViewColumn({ name: "tm_debut_vacation" })
  tmDebutVacation: Date;

  @ViewColumn({ name: "tm_fin_vacation" })
  tmFinVacation: Date;

  @ViewColumn({ name: "nb_heures_vacation" })
  nbHeuresVacation: number;
}
