import { ViewColumn, ViewEntity } from "typeorm";

@ViewEntity("v_rapport_absences_comete_bleu", { schema: "dbo", database: "dwh_seris"})
export class RapportAbsencesCometeBleu {
  @ViewColumn({ name: "id_matricule" })
  idMatricule: number

  @ViewColumn({ name: "li_nom" })
  liNom: string

  @ViewColumn({ name: "li_prenom" })
  liPrenom: string

  @ViewColumn({ name: "li_agence" })
  liAgence: string

  @ViewColumn({name: "li_code_agence_ax"})
  liCodeAgenceAx: string

  @ViewColumn({ name: "id_bu" })
  idBu: number

	@ViewColumn({ name: "li_code_societe_ax" })
	liCodeSocieteAx: string;

	@ViewColumn({ name: "li_site_principal" })
	liSitePrincipal: string;

  @ViewColumn({ name: "li_societe" })
  liSociete: string

  @ViewColumn({ name: "li_region" })
  liRegion: string

  @ViewColumn({ name: "id_region" })
  idRegion: number

  @ViewColumn({ name: "dt_debut_absence" })
  dtDebutAbsence: Date

  @ViewColumn({ name: "dt_fin_absence" })
  dtFinAbsence: Date

  @ViewColumn({ name: "li_type_absence" })
  liTypeAbsence: string

  @ViewColumn({ name: "li_source_donnee" })
  liSourceDonnee: string

	@ViewColumn({ name: "id_agence" })
    idAgence: number

	@ViewColumn({ name: "id_periode" })
    idPeriode: number

	@ViewColumn({ name: "li_periode" })
    liPeriode: string

}
