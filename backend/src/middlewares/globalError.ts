import { Request, Response, NextFunction } from "express";
import AppError from "../_errors/AppError";

export const globalError = (
  error: Error,
  request: Request,
  response: Response,
  __: NextFunction,
) => {
  if (error instanceof AppError) {
    const { statusCode, message } = error;

    return response.status(statusCode).json({
      message,
      statusCode,
    });
  }

  return response.status(500).json({
    message: "Internal server error 500.",
    statusCode: 500,
  });
};
