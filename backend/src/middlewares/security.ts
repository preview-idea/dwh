import { Request, Response, NextFunction } from "express";
import authConfig from "../config/auth";
import AppError from "../_errors/AppError";

export default function security(
  request: Request,
  response: Response,
  next: NextFunction,
): void {

  // TODO: check if client has access to this api
  // Code removed for security reasons

  next();
}
