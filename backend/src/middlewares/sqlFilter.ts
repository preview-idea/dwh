import { Request, Response, NextFunction } from "express";
import { Between, In } from "typeorm";
import { addYears, subYears } from "date-fns";

// TypeORM query operators
export const AfterDate = (date: Date) => Between(date, addYears(date, 100));
export const BeforeDate = (date: Date) => Between(subYears(date, 100), date);

// e.g { where: { date: AfterDate(new Date()) } }

export const sqlFilter = (
  request: Request,
  response: Response,
  next: NextFunction,
): void => {

  // TODO: mapping query params to typeorm query operators
  // Code removed for security reasons

  next();
};
