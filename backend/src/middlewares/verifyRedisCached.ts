import { Request, Response, NextFunction } from "express";
import { redisClient } from "../config/redis";

export const verifyRedisCached = (
  request: Request,
  response: Response,
  next: NextFunction,
): void => {

  // TODO: verify if cache exists in redis
  // Code removed for security reasons

  next();

};
