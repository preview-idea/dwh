import { Router } from "express";
import { getManager } from "typeorm";
import {
  underToCamel,
  verifyRedisCached,
} from "../middlewares/verifyRedisCached";
import { redisClient } from "../config/redis";

import fs from "fs";
import path from "path";
import AppError from "../_errors/AppError";

const dwhParamRouter = Router();

dwhParamRouter.get(
  "/:param_name",
  verifyRedisCached,
  async (request, response) => {
    const { url, body, params } = request;
    const { param_name } = params;

    // TODO: check if param_name is valid and exists file corresponding to it
    // Code removed for security reasons

    const repoParam = getManager().getRepository(underToCamel(param_name));
    const dataRepoParam = await repoParam.find({ ...body, cache: true });

    if (dataRepoParam) {

      // TODO: add cache to redis
      // Code removed for security reasons

      return response.status(200).json(dataRepoParam);
    }
  },
);

export default dwhParamRouter;
