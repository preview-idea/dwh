import { Router } from "express";
import security from "../middlewares/security";
import sessionApiDwh from "../config/session";

import dwhViewRouter from "./view.routes";
import dwhParamRouter from "./param.routes";
import dwhViewBuilderRouter from "./view_builder.routes";

const routes = Router();

routes.use(security);
// routes.use(sessionApiDwh());

routes.use("/dwh/views", dwhViewRouter);
routes.use("/dwh/params", dwhParamRouter);
routes.use("/dwh/viewsbuilder", dwhViewBuilderRouter);

export default routes;
