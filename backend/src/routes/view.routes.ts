import { Router } from "express";
import { getManager } from "typeorm";
import {
  underToCamel,
  verifyRedisCached,
} from "../middlewares/verifyRedisCached";
import { redisClient } from "../config/redis";
import { sqlFilter } from "../middlewares/sqlFilter";

import fs from "fs";
import path from "path";
import AppError from "../_errors/AppError";

const dwhViewRouter = Router();

dwhViewRouter.get(
  "/:view_name",
  sqlFilter,
  verifyRedisCached,
  async (request, response) => {
    const { url, body, params } = request;
    const { view_name } = params;

    // TODO: check if param_name is valid and exists file corresponding to it
    // Code removed for security reasons

    const repoView = getManager().getRepository(underToCamel(view_name));
    const dataRepoView = await repoView.find({ ...body, cache: true });

    if (dataRepoView) {

      // TODO: add cache to redis
      // Code removed for security reasons

      return response.status(200).json(dataRepoView);
    }
  },
);

export default dwhViewRouter;
