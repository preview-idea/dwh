import { Request } from "express";
import { sign } from "jsonwebtoken";
import authConfig from "../config/auth";
import { redisClient } from "../config/redis";

import AppError from "../_errors/AppError";

interface Response {
  token: string;
}

class AuthenticateClientService {
  public async execute({ ip, userClient }: Request): Promise<void> {

    // TODO: check if client has auth to access this api
    // Code removed for security reasons

  }
}

export default AuthenticateClientService;
