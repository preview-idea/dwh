declare namespace Express {
  export interface Request {
    userClient: {
      login: string;
      perimeter: string;
    };
  }
}
