import "reflect-metadata";
import "dotenv/config";

import express from "express";
import "express-async-errors";
import cors from "cors";
import compression from "compression";

import routes from "./routes";
import uploadConfig from "./config/upload";

import "./database";
import { globalError } from "./middlewares/globalError";

const app = express();

// Compress all response
app.use(compression());

// Allows access to the folder of upload (tmp)
app.use("/files", express.static(uploadConfig.directory));
app.use(cors());

// Allows HTTP Request on format json
app.use(express.json());
app.use(routes);
app.use(globalError);

export default app;
