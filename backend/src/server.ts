import app from "./app";

const SERVER_PORT = Number(process.env.APP_PORT);
const SERVER_HOST = process.env.APP_HOST

app.listen(SERVER_PORT, SERVER_HOST, () => {
  console.log(`🚀 Server started on port ${SERVER_PORT}`);
});
