export default {
  jwt: {
    origin_addr: process.env.ADDR_AUTH_CLIENT?.split(",") || [],
    secret_api: process.env.SECRET_API,
    expiresIn: "10h",
  },
};
