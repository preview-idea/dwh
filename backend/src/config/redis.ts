import redis from "redis";

export const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  port: Number(process.env.REDIS_PORT),
  password: process.env.REDIS_PASS,
  // Optional, if using SSL
  // use `fs.readFile[Sync]` or another method to bring these values in
  // tls: {
  //   key: 'stringValueOfKeyFile',
  //   cert: 'stringValueOfCertFile',
  //   ca: [ 'stringValueOfCaCertFile' ]
  // }
});

redisClient.on("connect", () => {
  console.log("Connected on redis server");
});
