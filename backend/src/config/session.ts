import session from "express-session";
import connectRedis from "connect-redis";
import { redisClient } from "./redis";
import { v4 as uuid } from "uuid";

const RedisStore = connectRedis(session);

export default function sessionApiDwh() {
  return session({
    genid: () => uuid(),
    name: "_sessionRedisApiDwh",
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, maxAge: 60000 }, // Note that the cookie-parser module is no longer needed
    store: new RedisStore({
      host: process.env.REDIS_HOST,
      port: Number(process.env.REDIS_PORT),
      pass: process.env.REDIS_PASS,
      client: redisClient,
      ttl: 86400,
    }),
  });
}
