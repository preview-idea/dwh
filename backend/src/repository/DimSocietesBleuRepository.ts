import { EntityRepository, Repository } from "typeorm";
import { DimSocietesBleu } from "../models/dwh/params/DimSocietesBleu";

@EntityRepository(DimSocietesBleu)
class DimSocietesBleuRepository extends Repository<DimSocietesBleu> {
  public async getListParam(body: object): Promise<DimSocietesBleu[]> {
    const param = await this.find({
      select: ["idSociete", "liSociete"],
      ...body,
      order: { liSociete: "ASC" },
      cache: true,
    });

    return param;
  }
}

export default DimSocietesBleuRepository;
